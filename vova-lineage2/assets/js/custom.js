$(document).ready(function(){
    "use strict";

    // 1. Scroll To Top 
        $(window).on('scroll',function () {
            if ($(this).scrollTop() > 600) {
                $('.return-to-top').fadeIn();
            } else {
                $('.return-to-top').fadeOut();
            }
        });
        $('.return-to-top').on('click',function(){
                $('html, body').animate({
                scrollTop: 0
            }, 1500);
            return false;
        });
    
    // 2. Smooth Scroll spy
        
        $('.header-area').sticky({
           topSpacing:0
        });
        
        //=============

        // $('.skydropdown-list > li > a, .try-it-btn').bind("click", function(event) {
        //     event.preventDefault();
        //     var anchor = $(this);
        //     $('html, body').stop().animate({
        //         scrollTop: $(anchor.attr('href')).offset().top - 0
        //     }, 1200,'easeInOutExpo');
        // });
        
        // $('body').scrollspy({
        //     target:'.navbar-collapse',
        //     offset:0
        // });


    // 3. welcome animation support

        $(window).load(function(){
            $(".header-text > div:first-child").removeClass("animated fadeInUp").css({'opacity':'0'});
            $(".header-text > div:last-child").removeClass("animated fadeInDown").css({'opacity':'0'});
        });

        $(window).load(function(){
            $(".header-text > div:first-child").addClass("animated fadeInUp").css({'opacity':'0'});
            $(".header-text > div:last-child").addClass("animated fadeInDown").css({'opacity':'0'});
        });

        // Menu
        if ($(window)['width']() < 767) {
            $('.skydropdown-list')['addClass']('skydown-mob');
            $(".skydropdown-list").append("<li></li>");
            $(".wrapper-lang").appendTo(".skydropdown > ul > li:last-child");
            $(".welcome-hero").insertAfter(".top-wrapper");
            // $(".selectpicker").appendTo(".header-area .row > div:first-child");
        }

        // Add fav-icons to menu
        if ($(window)['width']() > 767) {
            $('.skydropdown > ul > li').not(":last").append($('<i class="fal fa-diamond"></i>'));
            $('.skydropdown-list .skydropdown-submenu li.menu-item-has-children').mouseover(function() {
              $(this).addClass('permahover');
            });

            $('.skydropdown-list .skydropdown-submenu li.menu-item-has-children').mouseout(function() {
              $(this).removeClass('permahover');
            });
        }
        
        document['addEventListener']('touchstart', function() {}, false);
        $('.skydropdown')['append']($('<a class="skydropdown-anim-arrw"><span></span><span></span><span></span><span></span><span></span><span></span></a>'));
        $('.skydropdown')['append']($('<div class="skydropdown-text">Menu</div>'));
        $('.skydropdown-list > li')['has']('.skydropdown-submenu')['prepend']('<span class="skydropdown-click"><i class="skydropdown-arrow fa fa-angle-down"></i></span>');
        $('.skydropdown-submenu > li')['has']('ul')['prepend']('<span class="skydropdown-clk-two"><i class="skydropdown-arrow fa fa-angle-down"></i></span>');
        $('.skydropdown-submenu-sub > li')['has']('ul')['prepend']('<span class="skydropdown-clk-two"><i class="skydropdown-arrow fa fa-angle-down"></i></span>');
        $('.skydropdown-submenu-sub-sub > li')['has']('ul')['prepend']('<span class="skydropdown-clk-two"><i class="skydropdown-arrow fa fa-angle-down"></i></span>');
        $('.skydropdown-list li')['has']('.menu-big')['prepend']('<span class="skydropdown-click"><i class="skydropdown-arrow fa fa-angle-down"></i></span>');

        $('.skydropdown-anim-arrw')['click'](function() {
            $('.skydropdown-list')['slideToggle']('slow');
            $(this)['toggleClass']('skydropdown-lines')
        });
        $('.skydown-mob a')['click'](function() {
            $('.skydropdown-list')['slideToggle']('slow');
            $('.skydropdown-anim-arrw')['toggleClass']('skydropdown-lines')
        });

        $('.skydropdown-click')['click'](function() {
            $(this)['toggleClass']('skydropdownarrow-rotate')['parent']()['siblings']()['children']()['removeClass']('skydropdownarrow-rotate');
            $('.skydropdown-submenu, .menu-big')['not']($(this)['siblings']('.skydropdown-submenu, .menu-big'))['slideUp']('slow');
            $(this)['siblings']('.skydropdown-submenu')['slideToggle']('slow');
            $(this)['siblings']('.menu-big')['slideToggle']('slow')
        });
        $('.skydropdown-clk-two')['click'](function() {
            $(this)['toggleClass']('skydropdownarrow-rotate')['parent']()['siblings']()['children']()['removeClass']('skydropdownarrow-rotate');
            $(this)['siblings']('.skydropdown-submenu')['slideToggle']('slow');
            $(this)['siblings']('.skydropdown-submenu-sub')['slideToggle']('slow');
            $(this)['siblings']('.skydropdown-submenu-sub-sub')['slideToggle']('slow')
        });

        // This is just for the case that the browser window is resized
        window['onresize'] = function() {
            if ($(window)['width']() > 767) {
                $('.skydropdown-submenu')['removeAttr']('style');
                $('.skydropdown-list')['removeAttr']('style')
            }
        }

        // Language dropdown
        $('.lang-switcher').selectpicker({
            width: 'fit',
            style: 'null',
        });

        // Show more button
        $('.show-more .btn').click(function() {
          $('.news .row > div:hidden').slice(0, 2).fadeIn();
        });

        $('.owl-carousel').owlCarousel({
            items: 1,
            loop: true,
            video: true,
            lazyLoad: true
        }); 
}); 
    